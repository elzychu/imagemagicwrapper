/**
 * @author Piotr Zych 
 */

package net.garagefarm.imagemagicwrapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class CommandLine {
	
	public static final int OK = 0;
	public static final int PROBLEM = 1;
	
	private String result;
	private String errors;
	
	abstract class StreamGobbler extends Thread {
		BufferedReader stream;
		
		public StreamGobbler(InputStream stream) {
			this.stream = new BufferedReader(new InputStreamReader(stream));
		}
		
		public void run() {
			String line;
			
			try {
				while((line = stream.readLine()) != null) {
					parse(line);
				}
			} catch (IOException e) { }
		}
		
		abstract void parse(String line);
	}
	
	public CommandLine() {
		flush();
	}
	
	public String getResult() {
		return this.result;
	}
	
	public String getErrorMessage() {
		return this.errors;
	}
	
	public int exec(List<String> args) {	
		flush();
		
		Runtime rt = Runtime.getRuntime();
		Process proc;
		try {
			String[] array = new String[args.size()];
			
			String stringArgs = "";
			for(String arg: args)
				stringArgs += arg + " ";
			
			System.out.println("EXEC>>" + stringArgs);
			proc = rt.exec(args.toArray(array));		
			StreamGobbler is = new StreamGobbler(proc.getInputStream()) {
				
				@Override
				void parse(String line) {
					result += line + "\n";
					System.out.println("IMAGEMAGIC>>" + result);
				}
			};
		
			StreamGobbler errstr = new StreamGobbler(proc.getErrorStream()) {
				
				@Override
				void parse(String line) {
					errors += line + "\n";

					System.out.println("IMAGEMAGIC_ERROR>>" + result);
				}
			};
			
			is.start();
			errstr.start();
			
			return proc.waitFor() == 0 ? OK : PROBLEM;
			
		} catch (IOException | InterruptedException e) { 
			return PROBLEM;
		}	
	}
	

	private void flush() {
		this.result = "";
		this.errors = "";
	}
}
