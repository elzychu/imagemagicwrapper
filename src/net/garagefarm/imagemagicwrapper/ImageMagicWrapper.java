/**
 * @author Piotr Zych 
 */

package net.garagefarm.imagemagicwrapper;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ImageMagicWrapper {
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;
	public static final int MAX_STRIPS = 0;
	public static final int MODO_STRIPS = 1;
	
	private static final String TASK_OK = "ok";
	private static final String TASK_FAILED ="failed";
	private static Path imageMagicPath;
	
	private ExecutorService executor = Executors.newCachedThreadPool();
	private int width = -1;
	private int height = -1;
	private int orientation;
	private int mode;
	private boolean isDimensionGiven;
	private Path output;
	
	private List<String> extensions;
	private List<String> names;
	private List<List<File>> files;
	
	private String resultLines = "";
	private String errorLines = "";
	private int step=0;
	private int stepOveral=0;
	
	/**
	 * Creates new ImageMagicWrapper object with given orientation.
	 * 
	 * @param orientation strips orientation 
	 * {@link ImageMagicWrapper#HORIZONTAL}, 
	 * {@link ImageMagicWrapper#VERTICAL}
	 */
	public ImageMagicWrapper(int orientation) {
		if(orientation < 0 || orientation > 1)
			throw new InvalidParameterException();
		
		this.orientation = orientation;
	}
	
	/**
	 * Creates new ImageMagicWrapper object with given orientation 
	 * and ExecutorService
	 * 
	 * @param orientation strips orientation 
	 * {@link ImageMagicWrapper#HORIZONTAL}, 
	 * {@link ImageMagicWrapper#VERTICAL}
	 */
	public ImageMagicWrapper(ExecutorService executor, int orientation) {
		this(orientation);
		this.executor = executor;
	}
	
	/**
	 * Sets the ImageMagic directory path.
	 * if not given wrapper takes default path from environment (if exists).
	 * @param path path to ImageMagic directory
	 * @throws InvalidParameterException when path is invalid
	 */
	public static void setImageMagicPath(Path path) {
		File dir = path.toFile();
		
		if(!dir.exists() || !dir.isDirectory())
			throw new InvalidParameterException("Given directory not exists");
		
		boolean flag1=false;
		boolean flag2=false;
		
		for(File tmpFile: dir.listFiles()) {
			if(tmpFile.getName().startsWith("convert"))
				flag1=true;
			if(tmpFile.getName().startsWith("composite"))
				flag2=true;
		}
		
		
		if(!flag1 || !flag2)
			throw new InvalidParameterException("Given dir is not ImageMagic "+
					"root directory or is incompleate");
		
		imageMagicPath = path;
	}

	/**
	 * Sets project path (strips should be inside), scan given directory,
	 * find all files that match to mode scheme and sort them.
	 * @param projectPath path to directory in which store strips
	 * @param projectType type of strips 
	 * 		{@link ImageMagicWrapper#MAX_STRIPS} for 3dsMax
	 * @throws InvalidParameterException when path is invalid
	 */
	public void setProjectPath(Path projectPath, int projectType) {
		if(!projectPath.toFile().exists() || !projectPath.toFile()
			.isDirectory())
			throw new InvalidParameterException("Given directory not exists");
		
		List<File> files = new ArrayList<File>();
		for(File tmpFile: projectPath.toFile().listFiles()) {
			if(tmpFile.isFile())
				files.add(tmpFile);
		}
		
		this.mode = projectType;
		this.files = new ArrayList<List<File>>();
		this.names = new ArrayList<String>();
		this.extensions = new ArrayList<String>();		
		setupProject(files, projectType);
		
		if(this.files.size() == 0)
			throw new InvalidParameterException("Given directory is empty");
		
		
		for(List<File> tmpFiles: this.files) {
			Collections.sort(tmpFiles, new Comparator<File>() {

				public int compare(File arg0, File arg1) {
					return String.valueOf(arg0.getAbsolutePath())
						.compareTo(arg1.getAbsolutePath());
				}
				
			});
			
		}
	}
	
	/**
	 * Sets strip width.
	 * @param width strip width
	 * @throws InvalidParameterException when width is less or equal 0
	 */
	public void setSliceWidth(int width) {
		if(width<1)
			throw new InvalidParameterException("Width must be grater or " +
				"equal 1");
		
		this.width = width;
	}
	
	/**
	 * Sets strip height.
	 * @param width strip height
	 * @throws InvalidParameterException when height is less or equal 0
	 */
	public void setSliceHeight(int height) {
		if(height<1)
			throw new InvalidParameterException("Height must be grater or " 
				+ "equal 1");
		
		this.height = height;
	}
	
	/**
	 * Sets output directory in which wrapper will store result files.
	 * @param output path to output directory
	 * @throws InvalidParameterException when directory is not exists and 
	 * wrapper can not do new directory in given place
	 */
	public void setOutput(Path output) {
		if(!output.toFile().exists() && !output.toFile().mkdir())
			throw new InvalidParameterException("Cannot create output dir");
		
		this.output = output;
	}
	
	/**
	 *
	 * @return dumped result string stream from executed applications
	 */
	public String getResultLines() {
		return this.resultLines;
	}
	
	/**
	 * @return dumped error string stream from executed applications
	 */
	public String getErrorLines() {
		return this.errorLines;
	}
	
	/**
	 * Merge strips taken from given project directory, and saves result to
	 * output directory. Paths, mode, orientation, width and height should be 
	 * given before this method will be invoked. If width and height are not 
	 * given then application scan first strip from collection to evalue 
	 * dimensions.
	 * @throws Exception when any problems occur while external applications 
	 * working.
	 */
	public void merge() throws Exception {
		final ImageMagicWrapper parent = this;
		
		List<Callable<String>> tasks = new ArrayList<Callable<String>>();
		
		for(int i=0; i<this.files.size(); ++i) {
			
			String fixedName = this.names.get(i) + this.extensions.get(i);
			Path output = this.output.resolve(Paths.get(fixedName));
			final List<File> tmpFiles = this.files.get(i);
			
			int totalWidth = 0;
			int totalHeight = 0;
			try {
				setupDimensions(tmpFiles.get(0));
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}		
			
			int sliceSize=0;
			switch(this.mode) {
			case MODO_STRIPS:
				if(!isDimensionGiven) {
					if(this.orientation == VERTICAL)
						this.width /= tmpFiles.size();
					else
						this.width /= tmpFiles.size();
				}
			case MAX_STRIPS:
				totalWidth = this.orientation == VERTICAL ? 
					this.width * tmpFiles.size() :
					this.width;
				totalHeight = this.orientation == VERTICAL ?
					this.height :
					this.height * tmpFiles.size();
				sliceSize = this.orientation == VERTICAL ?
					this.width : this.height;
				break;
			}
				
			final int finalTotalWidth = totalWidth;
			final int finalTotalHeight = totalHeight;
			final int finalSliceSize = sliceSize;
			final Path finalOutput = output;
			
			tasks.add(new Callable<String>() {

				@Override
				public String call() throws Exception {
					try {
						switch(parent.mode) {
						case MAX_STRIPS:
							mergeMax(tmpFiles.get(0).getAbsolutePath(), 
								finalTotalWidth, finalTotalHeight, 
								finalSliceSize, parent.orientation, tmpFiles, 
								finalOutput.toString());
							
							break;
						case MODO_STRIPS:
							mergeModo(tmpFiles.get(0).getAbsolutePath(), 
								finalTotalWidth, finalTotalHeight, 
								finalSliceSize, parent.orientation, tmpFiles, 
								finalOutput.toString());
							break;
						}
						return TASK_OK;
					} catch (Exception e) {
						e.printStackTrace();
						return TASK_FAILED + e.getMessage();
					}
				}
			});
		}
		
		List<Future<String>> results = executor.invokeAll(tasks);
		executor.shutdown();
		
		for(Future<String> result: results) {
			String message = result.get();
			if(message.startsWith(TASK_FAILED)) {
				throw new Exception(message.replaceFirst(TASK_FAILED, ""));
			}
		}
	}
	
	private void setupDimensions(File file) throws Exception {
		if(this.width <= 0 || this.height <= 0) {
			CommandLine cmdLine = new CommandLine();
			
			Path identify = null;
			
			if(imageMagicPath == null)
				identify = Paths.get("identify");
			else
				identify = imageMagicPath.resolve("identify"); 
			
			List<String> args = new ArrayList<String>();
			args.add("\"" + identify.toString() + "\"");
			args.add("-format");
			args.add("\"%w %h\"");
			args.add("\"" + file.getPath().toString() + "\"");
			
			if(cmdLine.exec(args) == CommandLine.PROBLEM)
				throw new Exception(cmdLine.getErrorMessage());
			String result = cmdLine.getResult().replace("\n", "");
			String[] dimensions = result.split(" ");
			
			this.width = Integer.parseInt(dimensions[0]);
			this.height = Integer.parseInt(dimensions[1]);
			
			this.isDimensionGiven = false;
		} else
			this.isDimensionGiven = true;
	}
	
	private void mergeModo(String firstImage, int totalWidth, int totalHeight, 
		int sliceSize, int orientation, List<File> images, String output) 
		throws Exception {
		
		CommandLine cmdLine = new CommandLine();
		
		Path convert = null;
		
		if(imageMagicPath == null)
			convert = Paths.get("convert");
		else
			convert = imageMagicPath.resolve("convert");
		
		List<String> args = new ArrayList<String>();
		args.add(convert + "");
		args.add(firstImage + "");
		args.add("-crop");
		if(orientation==VERTICAL)
			args.add(sliceSize + "x" + totalHeight + "+0+0");
		else
			args.add(totalWidth + "x" + sliceSize + "+0+0");
		
		args.add(output);
		incAndPrintProgress();
		
		if(cmdLine.exec(args) == CommandLine.PROBLEM)
			throw new Exception(cmdLine.getErrorMessage());
		
		args.clear();
		
		for(int i=1; i<images.size(); ++i) {
			args.add(convert + "");
			args.add(output + "");
			args.add(images.get(i)  + "");
			args.add("-crop");
			if(orientation==VERTICAL) {
				args.add(sliceSize + "x" + totalHeight + "+" + i*sliceSize + 
					"+0");
				args.add("+append");
			} else {
				args.add(totalHeight + "x" + sliceSize + "+0+" + i*sliceSize);
				args.add("-append");
			}
			args.add(output + "");
			
			if(cmdLine.exec(args) == CommandLine.PROBLEM) {
				this.resultLines += cmdLine.getResult();
				this.errorLines += cmdLine.getErrorMessage();
				throw new Exception(cmdLine.getErrorMessage());
			}
			

			this.resultLines += cmdLine.getResult();
			this.errorLines += cmdLine.getErrorMessage();
			
			incAndPrintProgress();
			args.clear();
		}		
	}
	
	private void mergeMax(String firstImage, int totalWidth, int totalHeight, 
		int sliceSize, int orientation, List<File> images, String output) 
		throws Exception {
		
		CommandLine cmdLine = new CommandLine();
		
		Path convert = null;
		Path composite = null;
		
		if(imageMagicPath == null) {
			composite = Paths.get("composite");
			convert = Paths.get("convert");
		} else {
			composite = imageMagicPath.resolve("composite");
			convert = imageMagicPath.resolve("convert");
		}
		
		List<String> args = new ArrayList<String>();
		
		args.add(convert + "");
		args.add(firstImage + "");
		args.add("-extent");
		args.add(totalWidth + "x" + totalHeight);
		args.add(output + "");
		
		incAndPrintProgress();
		
		if(cmdLine.exec(args) == CommandLine.PROBLEM)
			throw new Exception(cmdLine.getErrorMessage());
		
		
		args.clear();
		for(int i=1; i<images.size(); ++i) {
			args.add(composite +"");
			args.add("-geometry");
			
			String geometry = "";
			
			switch(orientation) {
			case HORIZONTAL:
				geometry+="+0+" + (sliceSize*i);			
				break;
			case VERTICAL:
				geometry+="+" + (sliceSize*i) + "+0";
				break;
			};
			
			args.add(geometry);
			args.add(images.get(i).getAbsolutePath() + "");
			args.add(output + "");
			args.add(output + "");
			
			if(cmdLine.exec(args) == CommandLine.PROBLEM) {
				this.resultLines += cmdLine.getResult();
				this.errorLines += cmdLine.getErrorMessage();
				throw new Exception(cmdLine.getErrorMessage());
			}
			

			this.resultLines += cmdLine.getResult();
			this.errorLines += cmdLine.getErrorMessage();
			
			incAndPrintProgress();
			args.clear();
		}
	}
	
	private void incAndPrintProgress() {

		int progressCache = evalueProgress(this.step, this.stepOveral);
		this.step++;
		int progress = evalueProgress(this.step, this.stepOveral);
		
		if(progress != progressCache)
			System.out.println("PROGRESS>>" + evalueProgress(step, stepOveral) 
				+ "%");
	}
	
	private static int evalueProgress(int step, int overalStep) {
		return (step*100)/overalStep;
	}
	
	private static String longestSubstring(List<File> filesList) {
		String longestSubstring = "";
		if(filesList.size() > 0)
			longestSubstring = filesList.get(0).getName();
		
		for(File file: filesList)
			longestSubstring = longestCommonSubstring(longestSubstring, 
				file.getName());

		return longestSubstring;
	}
	
	private static String longestCommonSubstring(String S1, String S2) {
		int Start = 0;
		int Max = 0;
		for (int i = 0; i < S1.length(); i++) {
			for (int j = 0; j < S2.length(); j++) {
				int x = 0;
				while (S1.charAt(i + x) == S2.charAt(j + x)) {
					x++;
					if (((i + x) >= S1.length()) || ((j + x) >= S2.length()))
						break;
				}
					
				if (x > Max) {
					Max = x;
					Start = i;
				}
			}
		}
		
		return S1.substring(Start, (Start + Max));
	}
	
	private void setupProject(List<File> files, int mode) {
		String longestSubstring = null;
		for(File file: files) {
			String fullFileName = file.getName();
			int dotPos = fullFileName.lastIndexOf('.');
			String ext = fullFileName.substring(dotPos);
			fullFileName = fullFileName.substring(0, dotPos);
			
			String fileName = null;
			
			switch(mode) {
			case MAX_STRIPS:
				fileName = getMaxFileName(fullFileName);
				break;
			case MODO_STRIPS:
				fileName = getModoFileName(fullFileName);
				break;
			default:
				if(longestSubstring == null)
					longestSubstring = longestSubstring(files);
				fileName = longestSubstring;
			}
			
			if(fileName != null) {
				int i = this.names.indexOf(fileName);
				
				if(i == -1){
					this.names.add(fileName);
					this.files.add(new ArrayList<File>());
					this.extensions.add(ext);
					i = this.names.size() -1;
				}
				
				List<File> tmpFiles = this.files.get(i);
				String tmpExt =this.extensions.get(i);
				
				if(tmpExt.equals(ext))
					tmpFiles.add(file);
				
				stepOveral++;
			}
		}
	}
	
	private String getModoFileName(String fullFileName) {
		final String COMMON_STRIP_REGEX = "(.)+_s[0-9]{4}_(.)+[0-9]{4}";
			
		if(fullFileName.matches(COMMON_STRIP_REGEX)) {
			String[] modules = fullFileName.split("_");
			
			if(modules.length < 3)
				return null;
			
			return modules[0] + "_" + modules[2];
		} else
			return null;
	}

	private String getMaxFileName(String fullFileName) {
		final String COMMON_STRIP_REGEX = "_STP[0-9]{5}_(.)+[0-9]{4}";
		final String PREFIX = "_STP[0-9]{5}_";
		
		if(fullFileName.matches(COMMON_STRIP_REGEX))
			return fullFileName.replaceAll(PREFIX, "");
		else
			return null;
	}
}
