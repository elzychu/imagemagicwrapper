/**
 * @author Piotr Zych 
 */

package net.garagefarm.imagemagicwrapper;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Loader {
	
	public static void printHelp() 
	{
		System.out.println("ImageMagickWrapper");
		System.out.println("GarageFarm.Net (C) 2013");
		System.out.println("Author: Piotr Zych");
		System.out.println("Usage: $java -jar ImageMagickWrapper [args]");
		System.out.println("  -p projectPath");
		System.out.println("  -o output");
		System.out.println("  -w width (optional)");
		System.out.println("  -h height (optional)");
		System.out.println("  -m ImageMagick path (if not given " +
			"ImageMagicWrapper will taki it from enviroment");
		System.out.println("  -V|H strips orientation (V vertical, H horizontal");
		System.out.println("  -MODO|MAX strips type");
	}
	
	public static void main(String[] args) {
		Path projectPath = null;
		Path outputPath = null;
		Path imageMagicPath = null;
		
		int mode = -1;
		int orientation = -1;
		
		int width = -1;
		int height = -1;
		
		try {
			for(int i=0; i<args.length; ++i) {
				switch(args[i]) {
				case "-p":
					try {
						projectPath = Paths.get(args[i+1]);
						i++;
					} catch (InvalidPathException e) {
						printHelp();
						return;
					}
					break;
					
				case "-o":
					try {
						outputPath = Paths.get(args[i+1]);
						i++;
					} catch (InvalidPathException e) {
						printHelp();
						return;
					}
					break;
				
				
				case "-w":
					try {
						width = Integer.parseInt(args[i+1]);
						i++;
					} catch(NumberFormatException e) {
						printHelp();
						return;
					}
					break;
					
				case "-h":
					try {
						height = Integer.parseInt(args[i+1]);
						i++;
					} catch(NumberFormatException e) {
						printHelp();
						return;
					}
					
					break;
				
				case "-m":
					try {
						imageMagicPath = Paths.get(args[i+1]);
						i++;
					} catch (InvalidPathException e) {
						printHelp();
						return;
					}
					break;
					
				case "-V":
					orientation = ImageMagicWrapper.VERTICAL;
					break;
				case "-H":
					orientation = ImageMagicWrapper.HORIZONTAL;
					break;
				case "-MAX":
					mode = ImageMagicWrapper.MAX_STRIPS;
					break;
				case "-MODO":
					mode = ImageMagicWrapper.MODO_STRIPS;
					break;
				default:
					printHelp();
					return;
				}	
			}
		} catch(Exception e) {
			printHelp();
			return;
		}
		
		if(orientation == -1 || mode == -1 || projectPath == null 
			|| outputPath == null) {
			
			printHelp();
			return;
		}
			
		ImageMagicWrapper.setImageMagicPath(imageMagicPath);
		ImageMagicWrapper wrapper = new ImageMagicWrapper(orientation);
		wrapper.setOutput(outputPath);
		wrapper.setProjectPath(projectPath, mode);
		
		if(width>0)
			wrapper.setSliceWidth(width);
		
		if(height>0)
			wrapper.setSliceHeight(height);
		
		try {
			wrapper.merge();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
